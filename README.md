# LIS 3781 - Advanced Database Management

## Trevor Swartzman

### LIS 3781 Requirements:

1. [A1 README.md](a1/README.md "My A1 README.md file")
	* Install Ampps
	* git command descriptions
	* ERD image
	* Bitbucket repo links
		* This assignment and
		* The required bitbucket station

2. [A2 README.md](a2/README.md "My A2 README.md file")
	* Create company and customer tables
	* Populate both tables with five records each
	* Practice granting user permissions

3. [A3 README.md](a3/README.md "My A3 README.md file")
	* Photos of Oracle code
	* Pictures of populated tables, in the Oracle enviroment

4. [P1 README.md](p1/README.md "My P1 README.md file")
	* ERD screenshot
	* SQL server code
	* Corresponding SQL statements

5. [A4 README.md](a4/README.md "My A4 README.md file")
	* Photo of ERD
	* SQL code
	
6. [A5 README.md](a5/README.md "My A5 README.md file")
	* ERD screenshot

7. [P2 README.md](p2/README.md "My P1 README.md file")
	* Screenshot of local MongoDB application
	* Screenshot of MongoDB shell command(s)
	
