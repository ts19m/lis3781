# Advanced Database Concepts

## Trevor Swartzman

### Project #1 Requirements:
1. Screenshot of my ERD
2. SQL code
3. Corresponding SQL statements

#### README.md file should include the following items:

# ERD for Project 1
1. ![ERD](../photos/p1ERD.png)

# SQL code for Project 1
1. ![code1](../photos/p1c1.png)
2. ![code2](../photos/p1c2.png)
3. ![code3](../photos/p1c3.png)
4. ![code4](../photos/p1c4.png)
5. ![code5](../photos/p1c5.png)
6. ![code6](../photos/p1c6.png)
7. ![code7](../photos/p1c7.png)
8. ![code8](../photos/p1c8.png)
9. ![code9](../photos/p1c9.png)
10. ![code10](../photos/p1c10.png)
11. ![code11](../photos/p1c11.png)
12. ![code12](../photos/p1c12.png)
13. ![code13](../photos/p1c13.png)
14. ![code14](../photos/p1c14.png)
15. ![code15](../photos/p1c15.png)

# SQL statements using the code
1. ![sql1](../photos/p1s1.png)
2. ![sql2](../photos/p1s2.png)
3. ![sql3](../photos/p1s3.png)
4. ![sql4](../photos/p1s4.png)
5. ![sql5](../photos/p1s5.png)



