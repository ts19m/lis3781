# Advanced Database Concepts

## Trevor Swartzman

### Assignment #4 Requirements:

* ![ERD photo](../photos/a4erd.png)

### MS SQL Server code photos

1. ![1](../photos/a41.png)

2. ![2](../photos/a42.png)

3. ![3](../photos/a43.png)

4. ![4](../photos/a44.png)

5. ![5](../photos/a45.png)

6. ![6](../photos/a46.png)

7. ![7](../photos/a47.png)

8. ![8](../photos/a48.png)
