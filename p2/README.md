# Advanced Database Concepts

## Trevor Swartzman

### Project #2 Requirements:

* ![1](../photos/p2server.png)

* ![MongoDB running](../photos/p2MDB.png)

1. Showing the total number of restaurants in the database
![1 SHELL command](../photos/p2.1.png)

2. Checking how many are A graded food locations
![2 SHELL command](../photos/p2.2.png)

3. Updating an existing restaurant's cusine with a timestamp
![3 SHELL command](../photos/p2.3.png)

* EXTRA: Batch template to fire up a user session with the local database
![.bat Script](../photos/p2bat.png)


