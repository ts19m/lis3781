
# Advanced Database Management

## Trevor Swartzman

### Assignment #2 Requirements:

![A2 Requirements](../photos/a2PDF.png) 

#### Assignment Screenshots:

*Screenshots of SQL code*:

![SQL CODE PART1](../photos/a2code1.png)
![SQL CODE PART2](../photos/a2code2.png)

*Screenshot of Populated tables*:

![Company](../photos/a2cmp.png)
![Customer](../photos/a2cus.png)
