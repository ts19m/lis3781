# Advanced Database Concepts

## Trevor Swartzman

### Assignment #3 Requirements:

*Oracle Code*

![customer](../photos/a3customer.png)
![commodity](../photos/a3commodity.png)
![order](../photos/a3order.png)
![inserts](../photos/a3inserts.png)

*Populated Tables with Oracle Solutions*

1. ![oracleVersion](../photos/a3q1.png)

2. ![allVersionInfo](../photos/a3q2.png)

3. ![user](../photos/a3q3.png)

4. ![time](../photos/a3q4.png)

5. ![privileges](../photos/a3q5.png)

6. ![userTables](../photos/a3q6.png)

7. ![describeAndSelects](../photos/a3q7.png)

8. ![cusInfo](../photos/a3q8.png)

9. ![cusInfo2](../photos/a3q9.png)

10. ![cus_num=3](../photos/a3q10.png)

11. ![cus_balGreater1000](../photos/a3q11.png)

12. ![commodities](../photos/a3q12.png)

13. ![cusInfo3](../photos/a3q13.png)

14. ![allOrdersMinusCereal](../photos/a3q14.png)

15. ![cusInfo4](../photos/a3q15.png)

16. ![cusInfo5](../photos/a3q16.png)

17. ![cusInfo6](../photos/a3q17.png)
 
18. ![cusInfo7](../photos/a3q18.png)

19. ![totalOrders](../photos/a3q19.png)

20. ![cusUnits](../photos/a3q20.png)

21. ![cusTotalCost](../photos/a3q21.png)

22. ![aggregateValues](../photos/a3q22.png)

23. ![cusInfo8](../photos/a3q23.png)

24. ![cusInfo9](../photos/a3q24.png)

25. ![DVDmodifier](../photos/a3q25.png)
