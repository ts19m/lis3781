# LIS 3781

## Trevor Swartzman

### Assignment #1 Requirements:

 ![a1 Requiremtns](../photos/a1reqs.png)

#### AMPPS Screenshot:
 
 ![AMPPS](../photos/ampps.png)

#### Git commands w/short descriptions:

1. git init - create an empty Git repository or reinitialize an existing one
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or local branch
7. git remove rm - removes a remote URL from your repository

#### Assignment Screenshots:

 ![a1 ERD](../photos/a1ERD.png)
